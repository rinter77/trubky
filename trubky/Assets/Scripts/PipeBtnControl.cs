﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PipeBtnControl : MonoBehaviour
{
    [Tooltip("Pipe that will be spawned when buttong is clicked")]
    public GameObject toSpawnObject;

    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
        if(mainCamera == null)
        {
            Debug.Log("Main camera not found");
        }

        if (toSpawnObject == null)
        {
            Debug.Log("No object has been asigned, asigning empty gameobject");
            toSpawnObject = new GameObject();
        } else
        {
            string type = toSpawnObject.GetComponent<ManipulatibleController>().pipeType;
            TextMeshProUGUI tmp = gameObject.GetComponentInChildren<TextMeshProUGUI>();
            if (tmp != null)
            {
                tmp.text = type;
            }
        }
    }
    public void SpawnObject()
    {
        Transform camTransform = mainCamera.transform;
        Vector3 position = camTransform.position;
        Vector3 direction = new Vector3(camTransform.forward.x, 0, camTransform.forward.z);
        direction.Normalize();
        position = position + 0.8f*direction;
        position.y -= 0.3f;

        GameObject instance = Instantiate(toSpawnObject);
        instance.transform.position = position;
        instance.transform.Rotate(new Vector3(90, 0, 0));
    }
}
