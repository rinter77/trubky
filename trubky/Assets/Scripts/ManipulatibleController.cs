﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManipulatibleController : MonoBehaviour
{
    public string pipeType;


    public bool isSource = false;
    public int distance = int.MaxValue;

    private List<ManipulatibleController> neighbours;
    private Renderer[] renderers;
    public float pressure;
    void Start()
    {
        renderers = GetComponentsInChildren<Renderer>();
        neighbours = new List<ManipulatibleController>();
        StartCoroutine(CheckPressureRaise());

    }

    public void AddNeighbour(ManipulatibleController neighbour)
    {
        if (!neighbours.Contains(neighbour))
        {
            neighbours.Add(neighbour);
            if (neighbour.distance < distance - 1)
            {
                distance = neighbour.distance + 1;
                Debug.Log("!My distance is " + distance.ToString());
            }
        }
    }
    public void RemoveNeighbour(ManipulatibleController toBeRemoved)
    {
        neighbours.Remove(toBeRemoved);
    }

    public void PropagateDistance()
    {
        Debug.Log("My distance is " + distance.ToString());
        int neighbourDistance = distance + 1;
        foreach(ManipulatibleController neighbour in neighbours)
        {
            if (neighbour.distance > neighbourDistance)
            {
                neighbour.distance = neighbourDistance;
                neighbour.PropagateDistance();
            }
        }
    }

    public List<ManipulatibleController> GetNeigbours() => neighbours;
    public float GetPressure() => pressure;

    public IEnumerator AddPressure(float pres)
    {
        pressure += pres;
        float red = pressure / 100;

        yield return new WaitForSeconds(0.25f);

        List<int> indexes = new List<int>();
        for (int i = 0; i < neighbours.Count; i++)
        {
            if(neighbours[i].distance > distance)
            {
                indexes.Add(i);
            }
        }
        foreach (int index in indexes)
        {
            neighbours[index].StartCoroutine(neighbours[index].AddPressure(pres / indexes.Count));
        }
    }

    private IEnumerator CheckPressureRaise(float oldPressure = 0)
    {
        if (pressure > 100)
        {
            Debug.Log("Pressure is too high!");
            StartCoroutine(CheckPressureDrop());
        }
        else
        {
            float antired = 1 - pressure / 100;
            ChangeColor(new Color(1, antired, antired));
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(CheckPressureRaise(pressure));
        }
    }

    private IEnumerator CheckPressureDrop()
    {
        float red = 0;
        int blinker = 1;
        while (true)
        {
            if (pressure <= 100)
            {
                Debug.Log("Pressure is okey");
                red = pressure / 100;
                StartCoroutine(CheckPressureRaise());
                break;
            }
            yield return new WaitForSeconds(0.05f);
            red += blinker * 0.05f;
            if( red > 1)
            {
                blinker = -1;
                red = 1;
            }
            else if(red < 0)
            {
                blinker = 1;
                red = 0;
            }
            ChangeColor(new Color(1, 1 - red, 1 - red));
        }
    }

    private void ChangeColor(Color color)
    {
        foreach(Renderer renderer in renderers)
        {
            renderer.material.SetColor("_Color", color);
        }
    }

}
