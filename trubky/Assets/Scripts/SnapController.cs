﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapController : MonoBehaviour
{

    public void SetTriggerOnChildren(bool isTrigger)
    {
        Collider[] colliders = GetComponentsInChildren<Collider>();
        foreach(Collider coll in colliders)
        {
            coll.isTrigger = isTrigger;
        }
    }
}
