﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Source : MonoBehaviour
{
    public float srcPressure = 100;
    private int layer;
    private ManipulatibleController manipulatible;
    private void Awake()
    {
        layer = gameObject.layer;
        manipulatible = gameObject.GetComponentInParent<ManipulatibleController>();
    }
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.gameObject.layer == layer)
        {
            manipulatible.distance = 0;
            manipulatible.PropagateDistance();
            manipulatible.StartCoroutine(manipulatible.AddPressure(srcPressure));
        }
    }

}
