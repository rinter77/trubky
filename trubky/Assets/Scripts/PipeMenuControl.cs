﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeMenuControl : MonoBehaviour
{
    public GameObject deactivate;
    private void Start()
    {
        if (deactivate == null)
            deactivate = gameObject;
    }

    public void DisableMenu()
    {
        deactivate.SetActive(false);
    }
}
