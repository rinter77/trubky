﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snapable : MonoBehaviour
{
    public AudioClip Success;
    public AudioClip Failure;

    private int layer;
    private Transform grandParent;
    // Start is called before the first frame update    
    private void Awake()
    {
        grandParent = gameObject.transform.parent.parent;
        layer = gameObject.layer;
    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void SetTrigger(bool isTrigger)
    {
        gameObject.GetComponent<Collider>().isTrigger = isTrigger;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == layer)
        {
            Debug.Log("Im called too");
            ManipulatibleController otheContr = other.gameObject.GetComponentInParent<ManipulatibleController>();
            ManipulatibleController thisController = gameObject.GetComponentInParent<ManipulatibleController>();

            otheContr?.AddNeighbour(thisController);
            thisController?.AddNeighbour(otheContr);

            AudioSource.PlayClipAtPoint(Success, transform.position);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == layer)
        {
            ManipulatibleController otheContr = other.gameObject.GetComponentInParent<ManipulatibleController>();
            ManipulatibleController thisController = gameObject.GetComponentInParent<ManipulatibleController>();

            otheContr?.RemoveNeighbour(thisController);
            thisController?.RemoveNeighbour(otheContr);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == layer) {           
            Vector3 toRootPos = transform.position - grandParent.position;
            grandParent.position = other.transform.position - toRootPos;
            Vector3 rotAxis = Vector3.Cross(gameObject.transform.up, other.transform.up);
            grandParent.RotateAround(other.transform.position, rotAxis, Vector3.Angle(gameObject.transform.up, other.transform.up) + 180);
        }
    }
}
