﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeController : MonoBehaviour
{
    private List<ManipulatibleController[]> pipeGraph;
    // Start is called before the first frame update
    void Start()
    {
        pipeGraph = new List<ManipulatibleController[]>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ConstructGraph()
    {
        pipeGraph.Clear();
        GameObject[] pipes = GameObject.FindGameObjectsWithTag("pipe");
        foreach(GameObject pipe in pipes)
        {
            List<ManipulatibleController> neighbours = pipe.GetComponent<ManipulatibleController>().GetNeigbours();
            int length = neighbours.Count;
            neighbours.Insert(0, pipe.GetComponent<ManipulatibleController>());
            pipeGraph.Add(neighbours.ToArray());
        }

    }
}
