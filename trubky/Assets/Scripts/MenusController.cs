﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;


public class MenusController : MonoBehaviour
{
    public SteamVR_Action_Boolean pipeMenu; //Grab Pinch is the trigger, select from inspecter
    public SteamVR_Input_Sources leftSrc = SteamVR_Input_Sources.LeftHand;//which controllerd
    public SteamVR_Input_Sources rightSrc = SteamVR_Input_Sources.RightHand;//which controllerd

    public GameObject pipeMenuRef;
    // Start is called before the first frame update
    void Start()
    {
        if (pipeMenu != null)
        {
            pipeMenu.AddOnStateDownListener(SwapPipeMenu, leftSrc);
            pipeMenu.AddOnStateDownListener(SwapPipeMenu, rightSrc);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SwapPipeMenu(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if (!pipeMenuRef.activeSelf)
        {
            pipeMenuRef.SetActive(true);
        }
        Vector3 position = new Vector3();
        Vector3 direction = new Vector3();
        Quaternion rotation = new Quaternion();
      
        if (fromSource == SteamVR_Input_Sources.RightHand)
        {
            Transform rightHand = Player.instance.rightHand.transform;
            position = rightHand.position;
            direction = rightHand.forward;
        }
        else if (fromSource == SteamVR_Input_Sources.LeftHand)
        {
            Transform leftHand = Player.instance.leftHand.transform;
            position = leftHand.position;
            direction = leftHand.forward;
        }

        position += 0.2f * direction;

        rotation.SetLookRotation(direction);

        pipeMenuRef.transform.position = position;
        pipeMenuRef.transform.rotation = rotation;
    }
}
